

/* First created by JCasGen Wed Oct 21 22:04:51 PDT 2015 */
package com.deep6.uima.annotations;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Instance of a named entity.
 * Updated by JCasGen Wed Oct 21 22:04:51 PDT 2015
 * XML source: /Users/andrewhall/Documents/qurius/components/compiled/annotator/stanford-ner-annotator/target/jcasgen/typesystem.xml
 * @generated */
public class NER extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(NER.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected NER() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public NER(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public NER(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public NER(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: NERType

  /** getter for NERType - gets type term for the covered text (person, location, etc).
   * @generated
   * @return value of the feature 
   */
  public String getNERType() {
    if (NER_Type.featOkTst && ((NER_Type)jcasType).casFeat_NERType == null)
      jcasType.jcas.throwFeatMissing("NERType", "com.deep6.uima.annotations.NER");
    return jcasType.ll_cas.ll_getStringValue(addr, ((NER_Type)jcasType).casFeatCode_NERType);}
    
  /** setter for NERType - sets type term for the covered text (person, location, etc). 
   * @generated
   * @param v value to set into the feature 
   */
  public void setNERType(String v) {
    if (NER_Type.featOkTst && ((NER_Type)jcasType).casFeat_NERType == null)
      jcasType.jcas.throwFeatMissing("NERType", "com.deep6.uima.annotations.NER");
    jcasType.ll_cas.ll_setStringValue(addr, ((NER_Type)jcasType).casFeatCode_NERType, v);}    
  }

    