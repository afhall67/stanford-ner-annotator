
/* First created by JCasGen Wed Oct 21 22:04:51 PDT 2015 */
package com.deep6.uima.annotations;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Instance of a named entity.
 * Updated by JCasGen Wed Oct 21 22:04:51 PDT 2015
 * @generated */
public class NER_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (NER_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = NER_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new NER(addr, NER_Type.this);
  			   NER_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new NER(addr, NER_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = NER.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("com.deep6.uima.annotations.NER");
 
  /** @generated */
  final Feature casFeat_NERType;
  /** @generated */
  final int     casFeatCode_NERType;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getNERType(int addr) {
        if (featOkTst && casFeat_NERType == null)
      jcas.throwFeatMissing("NERType", "com.deep6.uima.annotations.NER");
    return ll_cas.ll_getStringValue(addr, casFeatCode_NERType);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setNERType(int addr, String v) {
        if (featOkTst && casFeat_NERType == null)
      jcas.throwFeatMissing("NERType", "com.deep6.uima.annotations.NER");
    ll_cas.ll_setStringValue(addr, casFeatCode_NERType, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public NER_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_NERType = jcas.getRequiredFeatureDE(casType, "NERType", "uima.cas.String", featOkTst);
    casFeatCode_NERType  = (null == casFeat_NERType) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_NERType).getCode();

  }
}



    