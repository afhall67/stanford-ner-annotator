

/* First created by JCasGen Wed Oct 21 22:04:51 PDT 2015 */
package com.deep6.uima.annotations;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** Identifies
 * Updated by JCasGen Wed Oct 21 22:04:51 PDT 2015
 * XML source: /Users/andrewhall/Documents/qurius/components/compiled/annotator/stanford-ner-annotator/target/jcasgen/typesystem.xml
 * @generated */
public class PersonName extends NER {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(PersonName.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected PersonName() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public PersonName(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public PersonName(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public PersonName(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: properName

  /** getter for properName - gets The proper name of this person
   * @generated
   * @return value of the feature 
   */
  public String getProperName() {
    if (PersonName_Type.featOkTst && ((PersonName_Type)jcasType).casFeat_properName == null)
      jcasType.jcas.throwFeatMissing("properName", "com.deep6.uima.annotations.PersonName");
    return jcasType.ll_cas.ll_getStringValue(addr, ((PersonName_Type)jcasType).casFeatCode_properName);}
    
  /** setter for properName - sets The proper name of this person 
   * @generated
   * @param v value to set into the feature 
   */
  public void setProperName(String v) {
    if (PersonName_Type.featOkTst && ((PersonName_Type)jcasType).casFeat_properName == null)
      jcasType.jcas.throwFeatMissing("properName", "com.deep6.uima.annotations.PersonName");
    jcasType.ll_cas.ll_setStringValue(addr, ((PersonName_Type)jcasType).casFeatCode_properName, v);}    
  }

    