
/* First created by JCasGen Wed Oct 21 22:04:51 PDT 2015 */
package com.deep6.uima.annotations;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** Identifies
 * Updated by JCasGen Wed Oct 21 22:04:51 PDT 2015
 * @generated */
public class PersonName_Type extends NER_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (PersonName_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = PersonName_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new PersonName(addr, PersonName_Type.this);
  			   PersonName_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new PersonName(addr, PersonName_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = PersonName.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("com.deep6.uima.annotations.PersonName");
 
  /** @generated */
  final Feature casFeat_properName;
  /** @generated */
  final int     casFeatCode_properName;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getProperName(int addr) {
        if (featOkTst && casFeat_properName == null)
      jcas.throwFeatMissing("properName", "com.deep6.uima.annotations.PersonName");
    return ll_cas.ll_getStringValue(addr, casFeatCode_properName);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setProperName(int addr, String v) {
        if (featOkTst && casFeat_properName == null)
      jcas.throwFeatMissing("properName", "com.deep6.uima.annotations.PersonName");
    ll_cas.ll_setStringValue(addr, casFeatCode_properName, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public PersonName_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_properName = jcas.getRequiredFeatureDE(casType, "properName", "uima.cas.String", featOkTst);
    casFeatCode_properName  = (null == casFeat_properName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_properName).getCode();

  }
}



    