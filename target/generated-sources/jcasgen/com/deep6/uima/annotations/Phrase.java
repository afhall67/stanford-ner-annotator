

/* First created by JCasGen Wed Oct 21 22:04:51 PDT 2015 */
package com.deep6.uima.annotations;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Instance of a cannonical term.
 * Updated by JCasGen Wed Oct 21 22:04:51 PDT 2015
 * XML source: /Users/andrewhall/Documents/qurius/components/compiled/annotator/stanford-ner-annotator/target/jcasgen/typesystem.xml
 * @generated */
public class Phrase extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Phrase.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Phrase() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Phrase(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Phrase(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Phrase(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: cannonical

  /** getter for cannonical - gets cannonical term for the covered text.
   * @generated
   * @return value of the feature 
   */
  public String getCannonical() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_cannonical == null)
      jcasType.jcas.throwFeatMissing("cannonical", "com.deep6.uima.annotations.Phrase");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_cannonical);}
    
  /** setter for cannonical - sets cannonical term for the covered text. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setCannonical(String v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_cannonical == null)
      jcasType.jcas.throwFeatMissing("cannonical", "com.deep6.uima.annotations.Phrase");
    jcasType.ll_cas.ll_setStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_cannonical, v);}    
  }

    