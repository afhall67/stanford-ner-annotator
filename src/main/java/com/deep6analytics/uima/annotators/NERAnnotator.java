package com.deep6analytics.uima.annotators;

import com.deep6.uima.annotations.NER;
import com.deep6.uima.annotations.Phrase;
import com.deep6analytics.uima.resource.SharedModel;
import edu.stanford.nlp.util.Triple;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.descriptor.*;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import java.util.List;

import static org.apache.uima.fit.util.JCasUtil.selectCovered;

@ResourceMetaData(
        name = "7Class NER",
        description = "From the Stanford NLP Group, this Annotator identifies people, locations, institutions and other signification entities from text",
        vendor = "Stanford NLP Group",
        version = "1.7.0",
        copyright = "Commercial"
)
@OperationalProperties(
        modifiesCas = true,
        outputsNewCases = false
)
@TypeCapability(
        outputs = { "com.deep6.uima.annotations.NER"}
)
public class NERAnnotator extends org.apache.uima.fit.component.JCasAnnotator_ImplBase {

    public static final String MODEL_KEY = "Model";
    @ExternalResource(key = MODEL_KEY)
    private SharedModel model;

    /*
    private static final String serialisedClassifier = "/qurator/classifiers/english.muc.7class.distsim.crf.ser.gz";
    private AbstractSequenceClassifier<CoreLabel> classifier;// = CRFClassifier.getClassifierNoExceptions(serialisedClassifier);
    */

    public static final String PARAM_SKIP = "skipIfPreviousAnnotation";
    @ConfigurationParameter(name = PARAM_SKIP, mandatory = false, description = "TRUE means the Annotator will look for entities in previous annotations.  FALSE means it will only apply to the document text.")
    private boolean skipIfPreviousAnnotation;

    public static final String PARAM_CLASSIFIER_PATH = "classifierPath";
    @ConfigurationParameter(name = PARAM_CLASSIFIER_PATH, mandatory = false, description = "Location of the trained model file.")
    private String classifierPath;

    @Override
    public void initialize(UimaContext aContext) throws ResourceInitializationException {
        super.initialize(aContext);
        /*
        if (classifierPath != null) {
            classifier = CRFClassifier.getClassifierNoExceptions(classifierPath);
        } else {
            classifier = CRFClassifier.getClassifierNoExceptions(serialisedClassifier);
        }
        */
    }

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        String document = aJCas.getDocumentText();
        List<Triple<String, Integer, Integer>> triplesList = model.classifier.classifyToCharacterOffsets(document);
        for (Triple<String, Integer, Integer> entity : triplesList) {
            String type = entity.first;
            int start = entity.second;
            int end = entity.third;
            String phrase = document.substring(start, end);
            if (skipIfPreviousAnnotation) {
                // skip if named entity is already covered by an annotation of the same length
                boolean foundAnnotation = false;
                List<Phrase> longerAnnotations = selectCovered(aJCas, Phrase.class, start, end);
                for (Phrase p : longerAnnotations) {
                    if (p.getCoveredText().contains(phrase)) {
                        foundAnnotation = true;
                        break;
                    }
                }
                if (foundAnnotation) {
                    continue;
                }
            }
            NER annotation = new NER(aJCas);
            annotation.setBegin(start);
            annotation.setEnd(end);
            annotation.setNERType(type);
            annotation.addToIndexes();
        }

    }

}