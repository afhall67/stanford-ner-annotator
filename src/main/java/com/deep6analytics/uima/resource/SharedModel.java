package com.deep6analytics.uima.resource;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.SharedResourceObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by newton on 3/24/15.
 */
public class SharedModel implements SharedResourceObject {
    private static final Logger log = LoggerFactory.getLogger(SharedModel.class);
    public AbstractSequenceClassifier<CoreLabel> classifier;// = CRFClassifier.getClassifierNoExceptions(serialisedClassifier);
    private String uri;

    public void load(DataResource aData) {
        uri = aData.getUri().toString();
        log.info("Model URI {}", uri);

        classifier = CRFClassifier.getClassifierNoExceptions(uri);
    }

    public String getUri() { return uri; }
}
