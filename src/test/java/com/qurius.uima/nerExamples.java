package com.qurius.uima;

import com.deep6analytics.uima.annotators.NERAnnotator;
import com.deep6analytics.uima.resource.SharedModel;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.ExternalResourceFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.junit.Ignore;
import org.junit.Test;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

/**
 * Created by newton on 8/24/15.
 */
public class nerExamples {
    String classifierPath = "src/test/resources/ver_3_2_0/english.muc.7class.distsim.crf.ser.gz";

    @Ignore
    @Test
    public void uimaTest() throws Exception {
        AnalysisEngineDescription aed = createEngineDescription(NERAnnotator.class, "skipIfPreviousAnnotation",true);
        ExternalResourceFactory.createDependencyAndBind(aed, "Model", SharedModel.class, classifierPath);
        AggregateBuilder ab = new AggregateBuilder();
        ab.add(aed);
        AnalysisEngine ae = ab.createAggregate();

        JCas jCas = JCasFactory.createJCas();
        jCas.setDocumentText("Hi James Bond. It's time to eat chicken.");
        SimplePipeline.runPipeline(jCas, ae);

        jCas.getView(CAS.NAME_DEFAULT_SOFA);
        FSIterator<Annotation> itr = jCas.getAnnotationIndex().iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }
        System.out.println(jCas.getDocumentText());
    }

    @Ignore
    @Test
    public void classifierTest() {
        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifierNoExceptions(classifierPath);
    }
}
