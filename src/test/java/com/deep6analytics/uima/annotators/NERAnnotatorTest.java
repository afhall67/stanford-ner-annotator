package com.deep6analytics.uima.annotators;

import com.deep6analytics.ims.hadoop.mapreduce.CASAnnotatorMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.impl.CASCompleteSerializer;
import org.apache.uima.cas.impl.Serialization;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by buddha on 8/24/15.
 */
public class NERAnnotatorTest {
    private MapDriver mapDriver;
    private Configuration configuration;
    private String inputText = "The cat and the fiddle, the cow jumped over the moon. The little dog laughed to see such sport and the dish ran away with the spoon.";
    private String OUTPUT_CAS_FILE = "src/test/resources/output/ner.cas";
    private String BRING_THE_NOISE = "src/test/resources/data/bring_the_noise.txt";
    private static String MODEL_LOCATION = "src/test/resources/ver_3_2_0/english.muc.7class.distsim.crf.ser.gz";
    private String pe;
    private BytesWritable input;

    private static final String RECIPE_JSON = "" +
            "{\n" +
            "  \"id\":\"1\",\n" +
            "  \"runCount\":\"0\",\n" +
            "  \"name\":\"a recipe\",\n" +
            "  \"lastUpdated\":\"2015-06-11\",\n" +
            "  \"annotators\":\n" +
            "  [\n" +
            "    {\n" +
            "      \"name\":\"weighted dictionary\",\n" +
            "      \"className\":\"com.deep6analytics.uima.annotators.NERAnnotator\",\n" +
            "      \"configParameters\":\n" +
            "      [\n" +
            "        {\"key\":\"classifierPath\",\"value\":\"" + MODEL_LOCATION + "\",\"type\":\"STRING\"},\n" +
            "        {\"key\":\"skipIfPreviousAnnotation\",\"value\":\"TRUE\",\"type\":\"BOOLEAN\"},\n" +
            "      ]\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    private byte[] createCas(String text) throws Exception {
        JCas jCas = JCasFactory.createJCas();
        jCas.setDocumentText(text);
        jCas.setDocumentLanguage("en");

        // Serialization with type system
        CASCompleteSerializer casCompleteSerializer = Serialization.serializeCASComplete(jCas.getCasImpl());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputObjectStream = new ObjectOutputStream(outputStream);
        outputObjectStream.writeObject(casCompleteSerializer);

        return outputStream.toByteArray();
    }

    @Before
    public void setUp() throws Exception {
        pe = new String(Files.readAllBytes(Paths.get(BRING_THE_NOISE)));
        System.out.println(pe);
        configuration = new Configuration();
        configuration.set("deep6.json.recipe", RECIPE_JSON);
        configuration.set(CASAnnotatorMapper.ANALYSIS_ID, "1");
        configuration.set(CASAnnotatorMapper.DOCUMENT_UUID, "NER-DOC-UUID");
        configuration.set(CASAnnotatorMapper.DOCUMENT_FILENAME, "NER-DOC-FILENAME");
        //input = new BytesWritable(createCas(inputText));
        input = new BytesWritable(createCas(pe));
        mapDriver = MapDriver.newMapDriver(new CASAnnotatorMapper());
    }


    @Test
    public void testMapper() throws Exception {

        List<Pair<NullWritable, BytesWritable>> results = mapDriver.withInput(NullWritable.get(), input)
                .withConfiguration(configuration)
                .run();

        assertEquals(1, results.size());
        /* Check the content of the CAS */
        ByteArrayInputStream inputStream = new ByteArrayInputStream(results.get(0).getSecond().getBytes());
        ObjectInputStream inputObjectStream = new ObjectInputStream(inputStream);

        // deSerialization with type system
        Object inputObj = inputObjectStream.readObject();

        CASCompleteSerializer casCompleteSerializer = null;
        if (inputObj instanceof CASCompleteSerializer) {
            casCompleteSerializer = (CASCompleteSerializer) inputObj;
        } else {
            throw new IOException("Deserialized Object is not an instance of CASCompleteSerializer");
        }

        JCas jCas = JCasFactory.createJCas();
        Serialization.deserializeCASComplete(casCompleteSerializer, jCas.getCasImpl());

        String content = jCas.getDocumentText();

        Iterator<JCas> viewItr = jCas.getViewIterator();
        while(viewItr.hasNext()){
            System.out.println("view:" + viewItr.next().getViewName());
        }

        /* Check the Annotations */
        FSIterator iterator = jCas.getAnnotationIndex().iterator();

        while (iterator.hasNext()) {
            Annotation annotation = (Annotation) iterator.next();
            System.out.print("\n *** " + annotation.getCoveredText() + "\n");
            System.out.print(annotation.toString());
        }

        assertNotNull(content);
    }



    // use this to generate test cas files
    @Ignore
    @Test
    public void writeAvroToCasToFile() throws Exception {
        Pair<NullWritable, BytesWritable> record = (Pair) mapDriver
                .withInput(NullWritable.get(), input)
                .withConfiguration(configuration)
                .run()
                .get(0);
        com.google.common.io.Files.write(record.getSecond().getBytes(), new File(OUTPUT_CAS_FILE));
    }

}